package securityexample.model;

import securityexample.utils.Haversine;
import java.util.List;

public class Location implements Comparable<Location>{

    private String type;
    private List coordinates;

    public int compareTo(Location location){
        double distance = Haversine.haversine((double)this.coordinates.get(1), (double)this.coordinates.get(0),
                (double) location.getCoordinates().get(1),(double)location.getCoordinates().get(0)) ;
        if(distance<0)
            return -1;
        else if(distance>0)
            return 1;
        return 0;
    }

    public Location(String type, List coordinates) {
        this.type = type;
        this.coordinates = coordinates;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List coordinates) {
        this.coordinates = coordinates;
    }
}

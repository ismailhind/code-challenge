package securityexample.model;

import java.time.Duration;
import java.time.LocalTime;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import securityexample.utils.Utils;

@Document(collection = "Users")
public class User {

	@Id
	private String id;
	private String username;
	private String password;
	private String role;
	private  String email;
	private final List<DislikedShop> dislikedShops;
	private final List<Favorites> favorites;

	@JsonCreator
	public User(@JsonProperty("id") String id,
				@JsonProperty("first_name") String username,
				@JsonProperty("password") String password,
				@JsonProperty("role") String role,
				@JsonProperty("email") String email,
				@JsonProperty("dislikes")List<DislikedShop> dislikedShops,
				@JsonProperty("favorites")List<Favorites> favorites) {
		super();
		this.username = username;
		this.id = id;
		this.password = password;
		this.role = role;
		this.email = username;
		this.dislikedShops = dislikedShops;
		this.favorites = favorites;
	}

	public List<DislikedShop> getDislikedShops() {
		return dislikedShops;
	}

	public List<Favorites> getFavorites() {
		return favorites;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}

	public void removeFromFavorites(String email){
		for (int i = 0; i < this.favorites.size(); i++) {
			if(this.favorites.get(i).getShopEmail().equals(email)){
				this.favorites.remove(i);
			}
		}
	}

	public void removeFromDislikes(String email){
		for (int i = 0; i < this.dislikedShops.size(); i++) {
			if(this.dislikedShops.get(i).getShopEmail().equals(email)){
				this.dislikedShops.remove(i);
			}
		}
	}

	public void addShopToFavorites(String shopEmail){
		this.favorites.add(new Favorites(shopEmail));
	}

	public void addShopToDislikes(String shopEmail){
		this.dislikedShops.add(new DislikedShop(shopEmail));
	}

	public void unshowPreferred(List<Favorites> favoritesList, List<Shop> userNearbyShops) {
		for (int i = 0; i < userNearbyShops.size(); i++) {
			if (favoritesList != null) {
				for (Favorites favorite : favoritesList) {
					if (userNearbyShops.get(i).getEmail().equals(favorite.getShopEmail())) {
						userNearbyShops.remove(i);
					}
				}
			}
		}
	}

	public void unshowDislikesWithin(User user, List<DislikedShop> dislikedShops, List<Shop> nearbyShops) {
		for (int i = 0; i < nearbyShops.size(); i++) {
			if (dislikedShops != null) {
				for (DislikedShop dislikedShop : dislikedShops) {
					//Test for just 10 seconds
					if (nearbyShops.get(i).getEmail().equals(dislikedShop.getShopEmail())
							&& ( Duration.between(dislikedShop.getDislikeTime(),
							LocalTime.now()).toMillis()/1000.0 < Utils.TIME_DISLIKE))
					{
						user.removeFromDislikes(dislikedShop.getShopEmail());
						nearbyShops.remove(i);
					}
				}
			}
		}
	}
}


package securityexample.model;

public class Favorites {

    private String shopEmail;

    public Favorites(String shopEmail) {
        this.shopEmail = shopEmail;
    }

    public String getShopEmail() {
        return shopEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Favorites favorites = (Favorites) o;
        return shopEmail != null ? shopEmail.equals(favorites.shopEmail) : favorites.shopEmail == null;
    }

    @Override
    public int hashCode() {
        return shopEmail != null ? shopEmail.hashCode() : 0;
    }
}

package securityexample.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "shops")
public class Shop {

    @Id
    private final ObjectId id;
    private final String picture;
    private final String name;
    private final String email;
    private final String city;
    private final Location location;

    @JsonCreator
    public Shop(@JsonProperty("id") ObjectId id,
                @JsonProperty("picture") String picture,
                @JsonProperty("name") String name,
                @JsonProperty("email") String email,
                @JsonProperty("city") String city,
                @JsonProperty("location") Location location) {
        this.id = id;
        this.picture = picture;
        this.name = name;
        this.email = email;
        this.city = city;
        this.location = location;
    }

    public ObjectId getId() {
        return id;
    }

    public String getPicture() {
        return picture;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getCity() {
        return city;
    }

    public Location getLocation() {
        return location;
    }
}



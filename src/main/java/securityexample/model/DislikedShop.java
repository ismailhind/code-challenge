package securityexample.model;

import java.time.LocalTime;

public class DislikedShop {

    private String shopEmail;
    private  LocalTime dislikeTime ;

    public DislikedShop(String shopEmail) {
        this.shopEmail = shopEmail;
        this.dislikeTime = LocalTime.now();
    }

    public String getShopEmail() {
        return shopEmail;
    }

    public LocalTime getDislikeTime() {
        return dislikeTime;
    }
}

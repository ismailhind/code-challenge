package securityexample.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import securityexample.model.User;

public interface UserRepository extends MongoRepository<User, String>{
	public User findByUsername(String username);
	public User findByEmail(String email);
	public User findById(String id);
}

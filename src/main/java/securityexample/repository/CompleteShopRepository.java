package securityexample.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import securityexample.model.Shop;
import securityexample.model.Location;

public interface CompleteShopRepository extends MongoRepository<Shop, ObjectId> {

    Shop findByEmail(String email);
    Shop findByLocation(Location location);
}
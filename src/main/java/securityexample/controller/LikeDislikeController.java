package securityexample.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import securityexample.model.User;
import securityexample.repository.UserRepository;
import securityexample.utils.Utils;
import java.util.List;

@Controller
public class LikeDislikeController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value="/likedislike", method = RequestMethod.POST)
    public String getLikeDislike(Model model, @RequestParam(value = "likedislike", required = false) String likedislike)
    {
        List<String> list = Utils.getTokensWithCollection(likedislike);
        String id = list.get(1);
        String shopEmail = list.get(3);
        String likeOrDislike = list.get(4);
        User user = this.userRepository.findById(id);

        if(likeOrDislike.equals("like") && !Utils.shopInfavorites(user,shopEmail)) {
            user.addShopToFavorites(shopEmail);
            this.userRepository.save(user);
        }
        if(likeOrDislike.equals("dislike") && !Utils.shopInDislikes(user,shopEmail)) {
            user.addShopToDislikes(shopEmail);
            this.userRepository.save(user);
        }

        model.addAttribute("id", user.getId());
        model.addAttribute("email", user.getEmail());
        return "forward:nearbyShops.jsp";
    }

    @RequestMapping(value="/remove", method = RequestMethod.POST)
    public String removeShopFromPreffered(Model model, @RequestParam(value = "emailandid", required = false) String emailandid)
    {
        List<String> list = Utils.getTokensWithCollection(emailandid);
        String id = list.get(1);
        String email = list.get(3);

        User user = this.userRepository.findById(id);
        user.removeFromFavorites(email);
        this.userRepository.save(user);

        model.addAttribute("id", user.getId());
        model.addAttribute("email", user.getEmail());
        return "forward:preferredShops.jsp";
    }
}

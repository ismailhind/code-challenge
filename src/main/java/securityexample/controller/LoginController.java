package securityexample.controller;

import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import securityexample.repository.UserRepository;
import securityexample.utils.Utils;

@Controller
public class LoginController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping("/signin")
    public String getSingUpPage(Model model) {
        Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>)
                SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        boolean hasRole = false;
        for (GrantedAuthority authority : authorities) {
            hasRole = authority.getAuthority().equals("USER");
            if (hasRole) break;
        }
        if (hasRole) {
            Utils.sendData(model, this.userRepository);
            return "forward:nearbyShops.jsp";
        } else {
            return "forward:signup.jsp";
        }
    }

    @RequestMapping("/login")
    public String getPage(Model model) {

        Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>)
                SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        boolean hasRole = false;
        for (GrantedAuthority authority : authorities) {
            hasRole = authority.getAuthority().equals("USER");
            if (hasRole)
                break;
        }
        if (hasRole) {
            Utils.sendData(model, this.userRepository);
            return "forward:nearbyShops.jsp";
        } else {
            return "forward:signup.jsp";
        }
    }
}

package securityexample.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import securityexample.repository.UserRepository;
import securityexample.utils.Utils;

@Controller
public class NearbyPreferredController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping("/nearbyShops")
    public String getUserNearbyShopsPage(Model model) {

        Utils.sendData(model,this.userRepository);
        return "forward:nearbyShops.jsp";
    }

    @RequestMapping(value="/nearbyController", method = RequestMethod.GET)
    public String redirectToPreffered(Model model) {

        Utils.sendData(model,this.userRepository);
        return "forward:preferredShops.jsp";
    }

    @RequestMapping(value="/preferredController", method = RequestMethod.GET)
    public String redirectToNearby(Model model) {

        Utils.sendData(model,this.userRepository);
        return "forward:nearbyShops.jsp";
    }
}

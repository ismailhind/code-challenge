package securityexample.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import securityexample.model.User;
import securityexample.repository.UserRepository;
import java.util.ArrayList;

@Controller
public class SignInUpController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping("/signupController")
    public String signupController(Model model,
                                   @RequestParam(value = "username", required = false) String username,
                                   @RequestParam(value = "role", required = false) String role,
                                   @RequestParam(value = "email", required = false) String email,
                                   @RequestParam(value = "password", required = false) String password) {

        User user = this.userRepository.findByEmail(email);
        String ROLE = role.toUpperCase();

        if (user == null) {
            User currentUser = new User("" + (this.userRepository.count() + 1),
                email, password, ROLE, username, new ArrayList<>(),new ArrayList<>());
            this.userRepository.save(currentUser);
            model.addAttribute("id", currentUser.getId());
            model.addAttribute("email", email);
            return "redirect:/signin";
        } else {
            model.addAttribute("error", "Email already exist");
            return "forward:signup.jsp";
        }
    }
}

package securityexample.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import securityexample.model.Shop;
import securityexample.model.Location;
import securityexample.repository.CompleteShopRepository;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/shops")
public class ControllerService {

    @Autowired
    private CompleteShopRepository completeShopRepository;

    @RequestMapping("/all")
    public List<Shop> getAll(){
        List<Shop> shops = this.completeShopRepository.findAll();
        List<Location> locations = new ArrayList<>();

        for (Shop shop : shops) {
            locations.add(shop.getLocation());
        }
        Collections.sort(locations);
        List<Shop> sortedShops = new ArrayList<>();

        for (Location location : locations) {
            Shop shop = this.completeShopRepository.findByLocation(location);
            sortedShops.add(shop);
        }
        return shops;
    }
}

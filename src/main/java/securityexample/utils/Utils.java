package securityexample.utils;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import securityexample.model.DislikedShop;
import securityexample.model.Favorites;
import securityexample.model.User;
import securityexample.repository.UserRepository;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

public final class Utils {

    public static final double TIME_DISLIKE = 10;

    public static List<String> getTokensWithCollection(String str) {
        //Arrays.stream(str.split(",")).collect(Collectors.toList());
        return Collections.list(new StringTokenizer(str, ",")).stream()
                .map(token -> (String) token)
                .collect(Collectors.toList());
    }

    public static void sendData(Model model, UserRepository userRepository){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        User user = userRepository.findByUsername(name);
        if(user != null){
            model.addAttribute("id", user.getId());
            model.addAttribute("email", user.getEmail());
        }
    }

    public static boolean shopInfavorites(User user, String shopEmail) {
        List<Favorites> favorites = user.getFavorites();
        if(favorites.contains(new Favorites(shopEmail)))
            return true;
        else
            return false;
    }

    public static boolean shopInDislikes(User user, String shopEmail) {
        List<DislikedShop> dislikedShops = user.getDislikedShops();
        if(dislikedShops.contains(new DislikedShop(shopEmail)))
            return true;
        else
            return false;
    }

}

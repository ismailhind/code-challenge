package securityexample.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import securityexample.model.Shop;
import securityexample.model.DislikedShop;
import securityexample.model.Favorites;
import securityexample.model.User;
import securityexample.repository.CompleteShopRepository;
import securityexample.repository.UserRepository;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UsersService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CompleteShopRepository completeShopRepository;

    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = "application/json")
    public List<User> getAll() {
        List<User> users = this.userRepository.findAll();
        return users;
    }

    @RequestMapping(value = "/preferred", method = RequestMethod.GET, produces = "application/json")
    public List<Shop> getPreferredById(Model model) {

        List<Shop> userFavoriteShops = new ArrayList<>();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        User user = this.userRepository.findByUsername(name);

        if (user != null) {
            if (user.getFavorites() != null) {
                for (Favorites favorites : user.getFavorites()) {
                    if (this.completeShopRepository.findByEmail(favorites.getShopEmail()) != null) {
                        userFavoriteShops.add(this.completeShopRepository.findByEmail(favorites.getShopEmail()));
                    }
                }
            }
        }
        model.addAttribute("id", user.getId());
        model.addAttribute("email", user.getEmail());
        return userFavoriteShops;
    }

    @RequestMapping(value = "/nearby", method = RequestMethod.GET, produces = "application/json")
    public List<Shop> getNearbyById( Model model) {

        List<Shop> nearbyShops = completeShopRepository.findAll();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        User user = this.userRepository.findByUsername(name);

        if (user != null) {
            List<Favorites> favoritesList = user.getFavorites();
            List<DislikedShop> dislikedShops = user.getDislikedShops();
            // delete preferred from list
            user.unshowPreferred(favoritesList, nearbyShops);
            // delete disliked shops (within 2 hours)
            user.unshowDislikesWithin(user, dislikedShops, nearbyShops);
            this.userRepository.save(user);
        }
        return nearbyShops;
    }
}

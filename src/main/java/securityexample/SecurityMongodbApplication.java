package securityexample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import securityexample.model.DislikedShop;
import securityexample.model.Favorites;
import securityexample.model.User;
import securityexample.service.UserService;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class SecurityMongodbApplication implements CommandLineRunner {

    @Autowired
    UserService service;

    public static void main(String[] args) {
        SpringApplication.run(SecurityMongodbApplication.class, args);
    }

    @Override
    public void run(String... arg0) throws Exception {
        service.deleteAll();
        List<Favorites> listFavorites = new ArrayList<>();
        List<DislikedShop> dislikedShops = new ArrayList<>();
        User user = new User("1", "user@gmail.com", "password", "USER",
                "user@gmail.com", dislikedShops, listFavorites);
        service.save(user);
    }
}

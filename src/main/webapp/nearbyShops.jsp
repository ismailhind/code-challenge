<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<head>
    <title>Title</title>




    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <link rel="stylesheet" href="css/style.css">
    <head>
<body class="container">
<div align="right">
    id = ${id}  ${email}<br>

    <form action="/logout" method="post">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        <button type="submit">Log out</button>
    </form>
</div>


<div align="left">
 <h3>   Nearby Shops <u><a href="nearbyController"> Preffered Shops</a></u></h3>
</div>

<div>
    <div ng-app="myApp" ng-controller="mainController" data-ng-init="refreshShopData(${id})">
        <div  ng-repeat="shop in shops | limitTo:5 ">
            <div class="col-md-3" style="padding-top: 50px;" style= "border-left: solid; border-top: solid;
                border-bottom: solid; border-right: solid">
                <table>
                    <tr ><td class="td">{{shop.name}}</td></tr>
                    <tr><td class="td"><img src="{{shop.picture}}" alt="{{shop.picture}}"></td></tr>
                    <tr>
                        <td class="td">
                            <form action="likedislike" method="post" >
                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                <button  name="likedislike" type="submit" value="id,${id},email,{{shop.email}},like" style="background-color: #4CAF50">Like</button>
                                <button name="likedislike" type="submit" value="id,${id},email,{{shop.email}},dislike"  style="background-color: #f44336">Dislike</button>
                            </form>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
<script  src="js/nearbyShops.js"></script>
</body>
</html>
var myApp = angular.module("myApp", []);
myApp.controller("mainController", function ($scope, $http) {

    $scope.shops = [];

    $scope.refreshShopData = function (id) {
        $http({
            method: 'GET',
            url: 'http://localhost:8080/users/preferred/'
        }).then(function successCallback(response) {
            $scope.shops = response.data;
        }, function errorCallback(response) {
            console.log(response.statusText);
        });
    }

});

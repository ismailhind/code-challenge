# Spring Security - MongoDB - Thymeleaf Example


How to compile it?
--------------------

You should have Maven tool and MongoDB to compile this project.


1) From the command line execute: 
    
    mongod
    
2) Go to  https://github.com/hiddenfounders/web-coding-challenge/blob/master/dump-shops.zip
    and download data for mongoDB
     
3) Extract the zip file then execute the command below :
   mongorestore --db shops shops/

4) From command line execute "mvn spring-boot:run"

5) You can sign in using: the email "user@gmail.com" and the password "password"

6) For more details about the project visit: 
    https://github.com/hiddenfounders/web-coding-challenge/blob/master/README.md




